public class Profesor implements Persona {
    /*************
     * Atributos
     *************/
    private String nombre;
    private String apellido;
    private String cedula;
    private double salario;
    private String materia;

    public Profesor(String nombre, String apellido, String cedula){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
    }

    

    public String getNombre() {
        return nombre;
    }



    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



    public String getApellido() {
        return apellido;
    }



    public void setApellido(String apellido) {
        this.apellido = apellido;
    }



    public String getCedula() {
        return cedula;
    }



    public void setCedula(String cedula) {
        this.cedula = cedula;
    }



    public double getSalario() {
        return salario;
    }



    public void setSalario(double salario) {
        this.salario = salario;
    }



    public String getMateria() {
        return materia;
    }



    public void setMateria(String materia) {
        this.materia = materia;
    }



    public void asignar_salon(){
        System.out.println("Salón asignado");
    }
}
