public class Alumno implements Persona{
    /*************
     * Atributos
     *************/
    private String nombre;
    private String apellido;
    private String cedula;
    private String curso;
    

    public Alumno(String nombre, String apellido, String cedula) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.curso = "";
    }

    

    public String getNombre() {
        return nombre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



    public String getApellido() {
        return apellido;
    }



    public void setApellido(String apellido) {
        this.apellido = apellido;
    }



    public String getCedula() {
        return cedula;
    }



    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void asignar_salon(){
        System.out.println("Salón asignado");
    }
}
